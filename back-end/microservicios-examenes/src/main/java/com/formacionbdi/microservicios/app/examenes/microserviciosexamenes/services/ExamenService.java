package com.formacionbdi.microservicios.app.examenes.microserviciosexamenes.services;

import java.util.List;

import com.formacionbdi.microservicios.commons.commonsmicroservicios.services.CommonService;
import com.formacionbdi.microservicios.commons.examenes.commonsexamenes.models.entity.Asignatura;
import com.formacionbdi.microservicios.commons.examenes.commonsexamenes.models.entity.Examen;

public interface ExamenService extends CommonService<Examen> {
    public List<Examen> findByNombre(String term);

    public Iterable<Asignatura> findAllAsignaturas();

    public Iterable<Long> findExamenesIdsConRespuestasByPreguntaIds(Iterable<Long> preguntaIds);
}
