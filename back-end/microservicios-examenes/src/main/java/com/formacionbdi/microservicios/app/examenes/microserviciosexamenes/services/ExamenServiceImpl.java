package com.formacionbdi.microservicios.app.examenes.microserviciosexamenes.services;

import java.util.List;

import com.formacionbdi.microservicios.app.examenes.microserviciosexamenes.models.repository.AsignaturaRepository;
import com.formacionbdi.microservicios.app.examenes.microserviciosexamenes.models.repository.ExamenRepository;
import com.formacionbdi.microservicios.commons.commonsmicroservicios.services.CommonServiceImpl;
import com.formacionbdi.microservicios.commons.examenes.commonsexamenes.models.entity.Asignatura;
import com.formacionbdi.microservicios.commons.examenes.commonsexamenes.models.entity.Examen;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ExamenServiceImpl extends CommonServiceImpl<Examen, ExamenRepository> implements ExamenService {

    @Autowired
    private AsignaturaRepository asignaturaRepository;

    @Override
    @Transactional(readOnly = true)
    public List<Examen> findByNombre(String term) {
        return repository.findByNombre(term);
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Asignatura> findAllAsignaturas() {
        return asignaturaRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Long> findExamenesIdsConRespuestasByPreguntaIds(Iterable<Long> preguntaIds) {
        return repository.findExamenesIdsConRespuestasByPreguntaIds(preguntaIds);
    }
    
}
