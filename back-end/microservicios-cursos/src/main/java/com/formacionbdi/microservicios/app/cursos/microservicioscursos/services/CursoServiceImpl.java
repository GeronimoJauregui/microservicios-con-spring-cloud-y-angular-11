package com.formacionbdi.microservicios.app.cursos.microservicioscursos.services;


import com.formacionbdi.microservicios.app.cursos.microservicioscursos.clients.AlumnoFeignClient;
import com.formacionbdi.microservicios.app.cursos.microservicioscursos.clients.RespuestaFeignClient;
import com.formacionbdi.microservicios.app.cursos.microservicioscursos.models.entity.Curso;
import com.formacionbdi.microservicios.app.cursos.microservicioscursos.models.repository.CursoRepository;
import com.formacionbdi.microservicios.commons.alumnos.commonsalumnos.models.entity.Alumno;
import com.formacionbdi.microservicios.commons.commonsmicroservicios.services.CommonServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CursoServiceImpl extends CommonServiceImpl<Curso, CursoRepository> implements CursosService {

    @Autowired
    private RespuestaFeignClient client;

    @Autowired
    private AlumnoFeignClient clientAlumno;

    @Override
    @Transactional(readOnly = true)
    public Curso findCursoByAlumnoId(Long id) {
        return repository.findCursoByAlumnoId(id);
    }

    @Override
    public Iterable<Long> obtenerExamenesIdsConRespuestasAlumno(Long alumnoId) {
        return client.obtenerExamenesIdsConRespuestasAlumno(alumnoId);
    }

    @Override
    public Iterable<Alumno> obtenerAlumnosPorCurso(Iterable<Long> ids) {
        return clientAlumno.obtenerAlumnosPorCurso(ids);
    }

    @Override
    @Transactional
    public void eliminarCursoAlumnoPorId(Long id) {
        repository.eliminarCursoAlumnoPorId(id);
    }
    
}
