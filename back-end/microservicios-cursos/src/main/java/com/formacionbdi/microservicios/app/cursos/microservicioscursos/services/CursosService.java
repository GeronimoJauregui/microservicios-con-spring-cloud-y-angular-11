package com.formacionbdi.microservicios.app.cursos.microservicioscursos.services;



import com.formacionbdi.microservicios.app.cursos.microservicioscursos.models.entity.Curso;
import com.formacionbdi.microservicios.commons.alumnos.commonsalumnos.models.entity.Alumno;
import com.formacionbdi.microservicios.commons.commonsmicroservicios.services.CommonService;


public interface CursosService extends CommonService<Curso> {
    public Curso findCursoByAlumnoId(Long id);

    public Iterable<Long> obtenerExamenesIdsConRespuestasAlumno(Long alumnoId);

    public Iterable<Alumno> obtenerAlumnosPorCurso(Iterable<Long> ids);

    public void eliminarCursoAlumnoPorId(Long id);
}
