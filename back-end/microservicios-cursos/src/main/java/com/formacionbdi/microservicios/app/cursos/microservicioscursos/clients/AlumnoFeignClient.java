package com.formacionbdi.microservicios.app.cursos.microservicioscursos.clients;

import java.util.List;

import com.formacionbdi.microservicios.commons.alumnos.commonsalumnos.models.entity.Alumno;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "microservicios-usuarios")
public interface AlumnoFeignClient {
    @GetMapping("/alumnos-por-curso")
    public List<Alumno> obtenerAlumnosPorCurso(@RequestParam Iterable<Long> ids);
}
