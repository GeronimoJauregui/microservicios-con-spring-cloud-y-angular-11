package com.formacionbdi.microservicios.app.usuarios.microserviciosusuarios.services;

import java.util.List;

import com.formacionbdi.microservicios.app.usuarios.microserviciosusuarios.client.CursoFeignClient;
import com.formacionbdi.microservicios.app.usuarios.microserviciosusuarios.models.repository.AlumnoRepository;
import com.formacionbdi.microservicios.commons.alumnos.commonsalumnos.models.entity.Alumno;
import com.formacionbdi.microservicios.commons.commonsmicroservicios.services.CommonServiceImpl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class AlumnoServiceImpl extends CommonServiceImpl<Alumno, AlumnoRepository> implements AlumnoService {

    @Autowired
    private CursoFeignClient clientCurso;

    @Override
    @Transactional(readOnly = true)
    public List<Alumno> findByNombreOrApellido(String term) {
        return repository.findByNombreOrApellido(term);
    }

    @Override
    @Transactional(readOnly = true)
    public Iterable<Alumno> findAllById(Iterable<Long> ids) {
        return repository.findAllById(ids);
    }

    @Override
    public void eliminarCursoAlumnoPorId(Long id) {
        clientCurso.eliminarCursoAlumnoPorId(id);
    }

    @Override
    @Transactional
    public void deleteById(Long id) {
        super.deleteById(id);
        this.eliminarCursoAlumnoPorId(id);
    }

    
    
}
