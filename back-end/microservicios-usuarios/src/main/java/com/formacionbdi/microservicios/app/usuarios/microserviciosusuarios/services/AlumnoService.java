package com.formacionbdi.microservicios.app.usuarios.microserviciosusuarios.services;

import java.util.List;

import com.formacionbdi.microservicios.commons.alumnos.commonsalumnos.models.entity.Alumno;
import com.formacionbdi.microservicios.commons.commonsmicroservicios.services.CommonService;

public interface AlumnoService extends CommonService<Alumno> {
    public List<Alumno> findByNombreOrApellido(String term);

    public Iterable<Alumno> findAllById(Iterable<Long> ids);

    public void eliminarCursoAlumnoPorId( Long id);
}
