package com.formacionbdi.microservicios.app.usuarios.microserviciosusuarios.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient("microservicios-cursos")
public interface CursoFeignClient {
    @DeleteMapping("/eliminar-alumno/{id}")
    public void eliminarCursoAlumnoPorId(@PathVariable Long id);
}
 